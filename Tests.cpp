#include <iostream>	                                        // for: std::cout and std::cerr
#include <fstream>	                                        // for: std::ifstream
#include <sstream>	                                        // for: std::stringstream
#include <string>	                                        // for: std::string, std::getline()
#include <iomanip>
#include <vector>
#include <chrono>
#include <time.h>
#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include "date_class.h"
#include "triangle.h"
using namespace std;

using testing::Eq;

namespace
{

    TEST(date_class, setDate)
    {
        date_class testDateClass;
        testDateClass.setDate("15-12-1992");

        ASSERT_EQ("15-12-1992", testDateClass.getDate());
    }

    TEST(date_class, addTime)
    {
        date_class testDateClass;                               //
        testDateClass.setDate("15-12-1992");
        testDateClass.addTime(259200);                          // 259200 sec = 3 dagen

        ASSERT_EQ("18-12-1992", testDateClass.getDate());
    }

    TEST(date_class, subTime)
    {
        date_class testDateClass;                               //
        testDateClass.setDate("15-12-1992");
        testDateClass.subTime(259200);                          // 259200 sec = 3 dagen

        ASSERT_EQ("12-12-1992", testDateClass.getDate());
    }

    TEST(triangle, test)
    {
        triangle testTriangleClass;                               //
        testTriangleClass.triangle_type(10, 10, 10, 45, 45, 90);
        ASSERT_EQ("Isosceles right",testTriangleClass.triangle_type(10, 10, 10, 45, 45, 90));
    }
}

