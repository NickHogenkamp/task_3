#include <iostream>
#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include <string>

#include "Tests/date_class.h"
#include "Tests/triangle.h"

using namespace std;

int main(int argc,char* argv[])
{


    testing::InitGoogleTest(&argc, argv);

    //triangle triangleObj;

    //string string1 = triangleObj.triangle_type(90, 90, 90, 45, 45, 90);
    //cout << string1 << endl;

    return RUN_ALL_TESTS();

    //cout << "\n\nTesting complete " << endl;
    //return 0;
}