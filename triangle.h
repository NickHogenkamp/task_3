#include <iostream>	                                        // for: std::cout and std::cerr
#include <fstream>	                                        // for: std::ifstream
#include <sstream>	                                        // for: std::stringstream
#include <string>	                                        // for: std::string, std::getline()
#include <iomanip>
#include <vector>
#include <chrono>
#include <time.h>
using namespace std;

#ifndef OPDRACHT2_TRIANGLE_H
#define OPDRACHT2_TRIANGLE_H


class triangle {
public:
    string triangle_type(double size1, double size2, double size3, double angle1, double angle2, double angle3);

private:
    void size(double size1, double size2, double size3, string &string1);               // Passes with reference
    void angle(double size1, double size2, double size3, string &string1);               // Passes with reference

};


#endif //OPDRACHT2_TRIANGLE_H
