//
// Created by hogen on 16-10-2018.
//
#include <iostream>	                                        // for: std::cout and std::cerr
#include <fstream>	                                        // for: std::ifstream
#include <sstream>	                                        // for: std::stringstream
#include <string>	                                        // for: std::string, std::getline()
#include <iomanip>
#include <vector>
#include <chrono>
#include <time.h>

#ifndef OPDRACHT3_DATE_CLASS_H
#define OPDRACHT3_DATE_CLASS_H
using namespace std;

class date_class {
public:
    date_class(void);
    void setDate(string dateString);                // Function to set a time in string format
    string getDate(void);
    string getCurrentDate(void);
    void addTime(long int timeToAdd);
    void subTime(long int timeToSub);

private:
    time_t time_t;
};


#endif //OPDRACHT3_DATE_CLASS_H
