#include <iostream>	                                        // for: std::cout and std::cerr
#include <fstream>	                                        // for: std::ifstream
#include <sstream>	                                        // for: std::stringstream
#include <string>	                                        // for: std::string, std::getline()
#include <iomanip>
#include <vector>
#include <chrono>
#include <time.h>
#include <math.h>       /* sin */

#include "triangle.h"

#define PI 3.14159265

string triangle::triangle_type(double size1, double size2, double size3, double angle1, double angle2, double angle3) {
    string string1;
    string string2;
    size(size1, size2, size3, string1);
    angle(angle1, angle2, angle3, string2);
    return (string1 + string2);
}


void triangle::size(double size1, double size2, double size3, string &string1){
    if (size1 == size2 == size3){
        string1 = "Equilateral ";
    }
    else if ((size1 == size2) || (size2 == size3) || (size1 == size3)){
        string1 = "Isosceles ";
    }else{
        string1 = "Scalene ";
    }
}


void triangle::angle(double angle1, double angle2, double angle3, string &string2){
    if ((angle1 ==90) || (angle2 == 90) || (angle3 == 90)){
        string2 = "right";
    }
    else if ((angle1 > 90) || (angle2 > 90) || (angle3 > 90)){
        string2 = "obtuse";
    }else{
        string2 = "acute";
    }
}