//
// Created by hogen on 16-10-2018.
//
#include <iostream>	                                        // for: std::cout and std::cerr
#include <fstream>	                                        // for: std::ifstream
#include <sstream>	                                        // for: std::stringstream
#include <string>	                                        // for: std::string, std::getline()
#include <iomanip>
#include <vector>
#include <chrono>
#include <time.h>
#include "date_class.h"

using namespace std;
date_class::date_class() {
    time(&time_t);
}

void date_class::setDate(string dateString) {
    tm structTime = {};
    std::istringstream iss(dateString);                     // just an output string stream
    iss >> std::get_time(&structTime, "%d-%m-%Y");          // output the date&time using the specified format
    time_t = mktime(&structTime);
}

string date_class::getDate(void) {
    tm structTime = *localtime(&time_t);
    std::ostringstream oss;                                 // just an output string stream
    oss << std::put_time(&structTime, "%d-%m-%Y");          // output the date&time using the specified format
    string string1 = oss.str();
    return string1;
}

string date_class::getCurrentDate() {
    time(&time_t);
    tm structTime = *localtime(&time_t);
    std::ostringstream oss;                                 // just an output string stream
    oss << std::put_time(&structTime, "%d-%m-%Y");          // output the date&time using the specified format
    return oss.str();
}

void date_class::addTime(long int timeToAdd){
    time_t = time_t + timeToAdd;
}

void date_class::subTime(long int timeToSub){
    time_t = time_t - timeToSub;
}
